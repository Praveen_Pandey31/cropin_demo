import io.restassured.RestAssured;
import io.restassured.filter.session.SessionFilter;
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.*;

import java.io.File;

import org.testng.Assert;
public class JIRA {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		RestAssured.baseURI="http://localhost:8090";
		//Start JIRA Session
		SessionFilter Session=new SessionFilter();
		String response= given().relaxedHTTPSValidation().log().all().contentType("application/json").body("{ \"username\": \"aishwaryashrivastava88\", \"password\": \"aishbhi0808\" }")
						.filter(Session).when().post("/rest/auth/1/session")
						.then().assertThat().log().all().statusCode(200).extract().response().asString();
		
		JsonPath js = new JsonPath(response);
	    String SessionName = js.getString("session.name");
	    String SessionValue = js.getString("session.value");
	    
	    System.out.println("name: "+SessionName+"Value: "+SessionValue);
	    //Create an issue using Session id
	    String issueResponse =given().log().all().contentType("application/json").body("{\r\n" + 
	    		"  \r\n" + 
	    		"  \"fields\": {\r\n" + 
	    		"    \"project\": {\r\n" + 
	    		"      \"key\": \"AISH\"\r\n" + 
	    		"    },\r\n" + 
	    		"    \"summary\": \"Rest Defect2\",\r\n" + 
	    		"    \"description\": \"Description Rest Defect2\",\r\n" + 
	    		"    \"issuetype\": {\r\n" + 
	    		"      \"name\": \"Bug\"\r\n" + 
	    		"    }\r\n" + 
	    		"  }\r\n" + 
	    		"}")
	    .filter(Session).when().post("/rest/api/2/issue")
	    .then().assertThat().log().all().statusCode(201).extract().response().asString();
	    JsonPath js1 = new JsonPath(issueResponse);
	    String IssueId = js1.getString("id");
	    System.out.println(IssueId);
	    String IssueKye = js1.getString("key");
	    String IssueSelf = js1.getString("self");
	    
	    //Create a comment on the issue
	    String comment="Aish Comment added .";
	    String commentResponse = given().log().all().contentType("application/json").pathParam("id", IssueId).body("{\r\n" + 
	    		"	\r\n" + 
	    		"	\"body\": \""+ comment+"\",\r\n" + 
	    		"  \"visibility\": {\r\n" + 
	    		"    \"type\": \"role\",\r\n" + 
	    		"    \"value\": \"Administrators\"\r\n" + 
	    		"  }\r\n" + 
	    		"  \r\n" + 
	    		"            \r\n" + 
	    		"  \r\n" + 
	    		"}").filter(Session).when().post("/rest/api/2/issue/{id}/comment")
	    		.then().log().all().assertThat().statusCode(201).extract().response().asString();
	    JsonPath js2 = new JsonPath(commentResponse);
	    String IssuecommentId = js2.getString("id");
	    System.out.println(IssuecommentId);
	    
	    //Add Attachment 
	    given().log().all().header("X-Atlassian-Token","no-check").contentType("multipart/form-data")
	    .pathParam("id", IssueId).multiPart("file",new File("jira.txt"))
	    .filter(Session).when().post("/rest/api/2/issue/{id}/attachments")
	    .then().log().all().assertThat().statusCode(200);
	    
	    //Get Issue Details
	    String IssueDetails = given().log().all().filter(Session).pathParam("id", IssueId).queryParam("fields","comment").when().get("/rest/api/2/issue/{id}").then().log().all().assertThat().statusCode(200).extract().response().asString();
	    JsonPath js3 = new JsonPath(IssueDetails);
	    int count =js3.get("fields.comment.comments.size()");
	    System.out.println(count);
	    for(int i=0;i<count;i++) {
	    	String id2= js3.get("fields.comment.comments["+i+"].id").toString();
	    	System.out.println(id2);
	    	if(id2.equalsIgnoreCase(IssuecommentId)) {
	    		String comment1= js3.get("fields.comment.comments["+i+"].body").toString();
	    		System.out.println(comment1);
	    	Assert.assertEquals(comment1, comment);
	    	}
	    	}
	    }
}

