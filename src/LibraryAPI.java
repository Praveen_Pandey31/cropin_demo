import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import Files.*;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.*;

public class LibraryAPI {
	
	@Test(dataProvider="bookData")
	public void AddBook(String insb, String aslie) {
		
		RestAssured.baseURI="http://216.10.245.166";
		String response = given().log().all().body(Payloads.addBook(insb,aslie)).contentType("application/json")
		.when().post("/Library/Addbook.php")
		.then().log().all().assertThat().statusCode(200).extract().asString();
		System.out.println(response);
		
		JsonPath js= reUseable.rawJason(response);
		String id=js.getString("ID");
		System.out.println(id);
		
		
		String resp=given().log().all().body(Payloads.deleteBook(id))
		.when().delete("/Library/DeleteBook.php")
		.then().log().all().assertThat().statusCode(200).extract().asString();
		
		System.out.println(resp);
		
	}
	
	@DataProvider(name= "bookData")
	public static Object[][] getData(){
		return new Object[][] {{"adshj","234"},{"dgdtrh","6457"},{"fregh","4346"}}; 
	}
	

}
