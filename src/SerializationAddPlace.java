import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

import java.util.ArrayList;
import java.util.List;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import pojo.AddPlace;
import pojo.Location;

public class SerializationAddPlace {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		RestAssured.baseURI="https://rahulshettyacademy.com";
		
		AddPlace ap = new AddPlace();
		ap.setAccuracy(50);
		ap.setAddress("Shrivastava Palace");
		ap.setLanguage("Hindi");
		ap.setName("Shobhita");
		ap.setPhone_number("3234587355");
		ap.setWebsite("www.shobhitashrivastav.com");
		List<String> myList = new ArrayList();
		myList.add("Shobhita");
		myList.add("Shrivastava");
		ap.setTypes(myList);
		Location l = new Location();
		l.setLat("36.56789");
		l.setLng("-34.32557");
		ap.setLocation(l);
		
		
		Response response=given().log().all().queryParam("key", "qaclick123").header("Content-Type", "application/json")
				.body(ap)
				.when().post("/maps/api/place/add/json")
				.then().log().all().assertThat().statusCode(200).body("scope",equalTo("APP"))
				.header("server", equalTo("Apache/2.4.18 (Ubuntu)")).extract().response();
		
		
	}

}
