import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

import java.util.ArrayList;
import java.util.List;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import pojo.AddPlace;
import pojo.Location;

public class SpecBuilderAddPlace {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//RestAssured.baseURI="https://rahulshettyacademy.com";
		
		AddPlace ap = new AddPlace();
		ap.setAccuracy(50);
		ap.setAddress("Shrivastava Palace");
		ap.setLanguage("Hindi");
		ap.setName("Shobhita");
		ap.setPhone_number("3234587355");
		ap.setWebsite("www.shobhitashrivastav.com");
		List<String> myList = new ArrayList();
		myList.add("Shobhita");
		myList.add("Shrivastava");
		ap.setTypes(myList);
		Location l = new Location();
		l.setLat("36.56789");
		l.setLng("-34.32557");
		ap.setLocation(l);
		
		RequestSpecification req = new RequestSpecBuilder().setBaseUri("https://rahulshettyacademy.com").addQueryParam("key", "qaclick123").addHeader("Content-Type", "application/json").build();
		RequestSpecification given = given().log().all().spec(req).body(ap);
		ResponseSpecification res = new ResponseSpecBuilder().expectStatusCode(200).build();
		
		
		Response response=//given().log().all().spec(req)
				//.queryParam("key", "qaclick123").header("Content-Type", "application/json").body(ap)
				given
				.when().post("/maps/api/place/add/json")
				.then().log().all().assertThat().spec(res).body("scope",equalTo("APP"))
				.header("server", equalTo("Apache/2.4.18 (Ubuntu)")).extract().response();
		//.statusCode(200)
		
	}

}
