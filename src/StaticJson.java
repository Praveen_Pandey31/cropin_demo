 import static io.restassured.RestAssured.given;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.testng.annotations.Test;
import Files.Payloads;
import Files.reUseable;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

public class StaticJson {
	
	@Test
	public static void addBook() throws IOException {
		RestAssured.baseURI="http://216.10.245.166";
		String response = given().log().all().body(GenerateStringFromResource("C:\\Users\\AISHWARYA\\Desktop\\Library.Json")).contentType("application/json")
		.when().post("/Library/Addbook.php")
		.then().log().all().assertThat().statusCode(200).extract().asString();
		System.out.println(response);
		
		JsonPath js= reUseable.rawJason(response);
		String id=js.getString("ID");
		System.out.println(id);
		
		
		String resp=given().log().all().body(Payloads.deleteBook(id))
		.when().delete("/Library/DeleteBook.php")
		.then().log().all().assertThat().statusCode(200).extract().asString();
	}
	
	public static String GenerateStringFromResource(String path) throws IOException {
		return new String(Files.readAllBytes(Paths.get(path)));
		
	}

}
