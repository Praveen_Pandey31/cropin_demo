import org.testng.Assert;
import org.testng.annotations.Test;

import Files.Payloads;
import io.restassured.path.json.JsonPath;

public class SumValidation {

	@Test
	public void sumofcourse() {
		JsonPath js = new JsonPath(Payloads.SelectCourse());// Raw Json
		//6. Verify if Sum of all Course prices matches with Purchase Amount
		int NoOfCourses=js.getInt("courses.size()");// No of Course.
		System.out.println(NoOfCourses);
		int finalAmount=0;
		for(int i=0;i<NoOfCourses;i++) {
			
			int price = js.getInt("courses["+i+"].price");
			int copies1 = js.getInt("courses["+i+"].copies");
			
			int amount = price*copies1;
			
			finalAmount = finalAmount + amount;  
		
		}
		
		System.out.println(finalAmount);
		Assert.assertEquals(910, finalAmount);
	}
	}

