import static io.restassured.RestAssured.given;

import org.testng.Assert;

import io.restassured.RestAssured;
import io.restassured.parsing.Parser;
import io.restassured.path.json.JsonPath;
import io.restassured.specification.RequestSpecification;

public class hubspot {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		RestAssured.baseURI = "https://api.hubapi.com";
		
		  String accessToken =
		  given().log().all().urlEncodingEnabled(false).queryParam("code", "5771f587-2fe7-40e8-8784-042fb4bc2c31").contentType("application/json")
		  .queryParam("client_id","ac76ffb0-e260-4f1d-b9ab-f59913e320ee")
		  .queryParam("client_secret", "b8b30936-a857-4165-b103-dc8385d5a844")
		  .queryParam("redirect_uri", "https://www.example.com/auth-callback")
		  .queryParam("grant_type", "authorization_code&")
		  .when().log().all().post("https://api.hubapi.com/oauth/v1/token").asString();
		 
		//https://app.hubspot.com/signup/developers/step/complete-verification?email=praveenpandey3192@gmail.com&token=aa840172e10a29f78fec7c424055eb2620a5e6849430909e5816fb4912f2012fa09c416e57acc21a625d6b18b3e1b385&didShowPartnerPrivacy=false&flowPath=developers&hubs_medium=referral&_ga=2.39153443.1802613489.1576611619-500942594.1573763828&preReferrer=https%3A%2F%2Flegacydocs.hubspot.com%2Fdocs%2Ffaq%2Fhow-do-i-create-a-test-account&hubs_source=https%3A%2F%2Flegacydocs.hubspot.com%2Fdocs%2Ffaq%2Fhow-do-i-create-a-test-account&nov=1&lang=en
		//String accessToken = given().log().all().urlEncodingEnabled(false).get("https://api.hubapi.com/oauth/v1/access-tokens/CJSP5qf1KhICAQEYs-gDIIGOBii1hQIyGQAf3xBKmlwHjX7OIpuIFEavB2-qYAGQsF4").asString();
		
		System.out.println(accessToken);
		JsonPath js= new JsonPath(accessToken);
		String token = js.get("access_token");
		
		String createCompany = given().relaxedHTTPSValidation().log().all().contentType("application/json").queryParam("hapikey","demo").queryParam("access_token", token).body("{\r\n" + 
				"  \"properties\": [\r\n" + 
				"    {\r\n" + 
				"      \"name\": \"name\",\r\n" + 
				"      \"value\": \"A company name1\"\r\n" + 
				"    },\r\n" + 
				"    {\r\n" + 
				"      \"name\": \"description1\",\r\n" + 
				"      \"value\": \"A company description1\"\r\n" + 
				"    }\r\n" + 
				"  ]\r\n" + 
				"}").when().post("/companies/v2/companies/")
				.then().assertThat().log().all().statusCode(201).extract().response().asString();
		
		System.out.println(createCompany);
		
		String companyName = given().log().all().contentType("application/json").queryParam("hapikey","demo").when().get("/companies/v2/companies/recent/created")
				.then().assertThat().log().all().statusCode(201).extract().response().asString();
		System.out.println(companyName);
		JsonPath js1= new JsonPath(companyName);
		
		String name_of_company = js1.get("results.properties.versions.name");
		Assert.assertEquals(name_of_company, "A company name1");
		
		
		
	}

}
