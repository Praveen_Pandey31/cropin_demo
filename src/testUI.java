

import java.util.Iterator;
import org.testng.annotations.Test;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;


public class testUI {
	
	WebDriver driver;
	JavascriptExecutor js;
	WebDriverWait wait;

	@Test(priority = 1)
		public void launchhubspot(){
		
		System. setProperty("webdriver.chrome.driver", "src/snippet/chromedriver.exe"); 	
		driver = new ChromeDriver();
		driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
		driver.navigate().to("https://www.hubspot.com/");
		driver.manage().window().maximize();
		js = (JavascriptExecutor) driver;  
		js.executeScript("window.scrollBy(0,100)");
		driver.findElement(By.linkText("Get HubSpot free")).click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		String PAgetitel= driver.getTitle();
		System.out.println(PAgetitel);
		WebElement elemt=driver.findElement(By.xpath("//body/div[3]/div[3]/section[1]/div[1]/div[4]"));
		js.executeScript("arguments[0].scrollIntoView(true);", elemt);
		driver.findElement(By.xpath("//*[contains(text(),'See features')]")).click();
		String text= driver.findElement(By.xpath("(//*[contains(text(),'Premium features')])[4]")).getText();
		System.out.println(text);
		List<WebElement> elementName = driver. findElements(By.xpath("(//div[@class='get-started-cards-variant__features-primary'])[4]/ul/li"));
		for(WebElement ele: elementName) {
			System.out.println(ele.getText());
		}
	}
	@Test(priority = 2)
	public void validfeaturesfordemo() throws Exception {
		Thread.sleep(3000);
		driver.findElement(By.xpath("(//a[contains(text(),'Get a demo')])[5]")).click();
		Thread.sleep(3000);
		WebElement firstname = driver.findElement(By.xpath("(//input[@name='firstname'])[5]"));
		if(firstname.isDisplayed() == true && firstname.isEnabled()== true) {
		firstname.click();
		firstname.sendKeys("Test");
		}
		driver.findElement(By.xpath("(//input[@name='lastname'])[5]")).sendKeys("Test");
		driver.findElement(By.xpath("(//input[@name='email'])[5]")).sendKeys("rebox88307@drluotan.com");
		driver.findElement(By.xpath("(//input[@name='phone'])[5]")).sendKeys("9999568702");
		driver.findElement(By.xpath("(//input[@name='company'])[5]")).sendKeys("workgropu");
		driver.findElement(By.xpath("(//input[@name='website'])[5]")).sendKeys("www.workgroup.com");
		WebElement dropdown=driver.findElement(By.xpath("(//select[@name='employees__c'])[5]"));
		Select sc = new Select(dropdown);
		sc.selectByValue("11 to 25");
		driver.findElement(By.xpath("(//input[@value='Get your free demo'])[4]")).click();
		WebElement button =driver.findElement(By.xpath("//a[@class='cta--primary cta--medium typdemo-videohero']"));
		Thread.sleep(40000);
		wait = new WebDriverWait(driver,40);
		wait.until(ExpectedConditions.visibilityOf(button));
		button.click();
	    WebElement signupheader=driver.findElement(By.xpath("(//*[contains(text(),'Create your free account')])"));
	    wait.until(ExpectedConditions.visibilityOf(signupheader));
	    
	}
@Test(priority = 3)
public void demoregestation() throws Exception {
	    driver.findElement(By.xpath("(//input[@name='FIRST_NAME'])")).sendKeys("Test");
	    driver.findElement(By.xpath("//input[@name='LAST_NAME']")).sendKeys("Test");		
	    driver.findElement(By.xpath("//input[@name='EMAIL']")).sendKeys("rebox88307@drluotan.com");
	    Thread.sleep(3000);
	    WebElement submitebutton= driver.findElement(By.xpath("//button[@type='submit']"));
	    js.executeScript("arguments[0].scrollIntoView(true);",submitebutton );
	    System.out.println(submitebutton.isEnabled());
	    if(submitebutton.isEnabled() == true) {
	    	submitebutton.click();
	    }
	    WebElement searchinduntry= driver.findElement(By.xpath("//*[contains(text(),'What industry are you in?')]"));
	    Thread.sleep(6000);
	    wait.until(ExpectedConditions.visibilityOf(searchinduntry));
	    WebElement industrysearch= driver.findElement(By.xpath("//input[@type='search']"));
	    industrysearch.sendKeys("Computer & Network Security");
	    driver.findElement(By.xpath("//button[@title='Computer & Network Security']")).click();
	    Thread.sleep(3000);
	    js.executeScript("window.scrollBy(0,100)");
	    Thread.sleep(3000);
//	    if(submitebutton.isEnabled() == true) {
//	    	submitebutton.click();
//	    }	
	    
		
		

	}

}
